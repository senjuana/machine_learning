function sBest=DE(nf)

%OBTENCION DE LIMITE INFERIOR, SUPERIOR, DIMENSION Y FUNCION OBJETIVO
[lb,ub,dim,fobj] = Get_Functions_details(nf);

%% PARAMETROS DEL ALGORITMO
n = 40;   %NUMERO DE INDIVIDUOS
CR = 0.9; %PROBABILIDAD DE MUTACION 
F = 0.5;  %OPERADOR DE CRUZAMIENTO
ni = 100; %NUMERO DE ITERACIONES

%% ALGORITMO 

tic 
figure(1)

%INICIALIZACION DE LAS VARIABLES
for i=1:n

        for var=1:dim
            xi(var,i)=lb+rand*(ub-lb);
        end

end

%ITERACIONES
for i=1:ni
    
    %POR CADA INDIVIDUO
    for j=1:n
        
       %CONDICIONES PARA CRUZAMIENTO 
       r0=j;
       while(r0==j || r0==0) 
          r0 = round(rand*n); 
       end
       
       r1=r0;
       while(r1==r0 || r1==j || r1==0)
          r1 = round(rand*n); 
       end
       
       r2=r1;
       while(r2==r1 || r2==r0 || r2==j || r2==0)
          r2 = round(rand*n); 
       end
        
        %VARIABLE A MUTAR
        vrand=round(rand*dim);
        while(vrand==0)
            vrand=round(rand*dim);
        end
        
        %MUTACION
        for var=1:dim
            
            if (rand<=CR || var==vrand)
                mutx(var,j)=xi(var,r0)+F*(xi(var,r1)-xi(var,r2));
                  if(mutx(var,j)<lb || mutx(var,j)>ub)
                       mutx(var,j)= lb+rand*(ub-lb); 
                  end
            else
                mutx(var,j)=xi(var,j);
            end
        
        end
        
    end
    
    
    %EVALUACION DE SOLUCIONES
    for k=1:n
        
       s(k)=fobj(xi(:,k));      %SOLUCION ANTES DE MUTACION
       muts(k)=fobj(mutx(:,k)); %SOLUCION DESPUES DE LA MUTACION
             
       %CONDICION DE MEJORA
       if(muts(k)<s(k))
          xi(:,k)= mutx(:,k);
       end
       
    end
   
       %GRAFICA DE RESPUESTA EN TIEMPO
       optim(i)=min(s);
       
end

hold on
plot(optim,'b')
xlabel('iteration');
ylabel('optimum');

%% BUSQUEDA DE LA MEJOR SOLUCION
[mutsmin indm] = min(muts);
[smin ind] = min(s);
bests = [mutsmin smin];
[sBest io] = min(bests);

if(1==io)
    xmin=mutx(:,indm);
else
    xmin=xi(:,ind);
end

%% RESULTADOS
disp(' ')
disp('---RESULTADOS DIFFERENTIAL EVOLUTION---')
fprintf('MINIMO GLOBAL EN EL RANGO: %f \n',sBest)
fprintf('VALOR DE LAS VARIABLES')
disp(xmin)
toc

end
