# About
Este  es un repositorio publico que existe para poder guardar mi proceso en el aprendizaje de multiples topicos de machine learning.

## Requirements

Supported operating systems:

* Development environments: GNU + Linux, MacOS X.

Requirements:

* Python >= 3.5


## Getting the code

    $ git clone https://gitlab.com/senjuana/machine_learning.git
    $ cd machine_learning


## Running Code
Para correr las implementaciones hechas en este repositorio es necesario tener algunas dependencias para instalarlas sigue los siguientes pasos:    
	
* Instala las dependencias:
    

    $ sudo pip install -r requirements.txt


* Corre cualquiera de los ejemplos del repositorio:


	$ ./linear-regresion.py

# Contributing
Este codigo sigue nuestra version de Contributor Covenant antes de hacer aportes al codigo lee con atencion las reglas.
[Contributing](https://gitlab.com/senjuana/machine_learning/blob/master/CONTRIBUTING.md)



# License
MIT License

Copyright (c) 2017 José Ernesto Ruiz Valdiva

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

# FAQ

* **Cual es el punto de compartir tu codigo?**

    Mi unico interes es el de poder compartir mis implementaciones de algoritmos de ML es como  herramnienta de aprendizaje.

* **Puedo utilizar este codigo en mis proyectos?**

    Siempre y cuando sigas los parametros de la licencia cualquiera puede utilizar o modiificar las implementaciones de este repositorio para cualquier proyecto personal.

* **Existe alguna documentacion teorica de los conceptos?**

    Efectivamente Puedes observar parte de la documentaion que yo e leido en los siguiente links:


