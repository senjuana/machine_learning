# Theoretical Concepts:

## Types of machine learning

### supervised learning:

A training set of examples with the correct responses (targets) is
provided and, based on this training set, the algorithm generalises to respond correctly
to all possible inputs. This is also called learning from exemplars.

### Unsupervised learning:

Correct responses are not provided, but instead the algorithm
tries to identify similarities between the inputs so that inputs that have something in
common are categorised together. The statistical approach to unsupervised learning is
known as density estimation.

### Reinforment learning:

This is somewhere between supervised and unsupervised learn-
ing. The algorithm gets told when the answer is wrong, but does not get told how to
correct it. It has to explore and try out different possibilities until it works out how to
get the answer right. Reinforcement learning is sometime called learning with a critic
because of this monitor that scores the answer, but does not suggest improvements.

### Evolutionary learning:

Biological evolution can be seen as a learning process: biological
organisms adapt to improve their survival rates and chance of having offspring in their
environment. We’ll look at how we can model this in a computer, using an idea of
fitness, which corresponds to a score for how good the current solution is.


## Terminology

### Inputs 
An input vector is the data given as one input to the algorithm. Written as x, with
elements x i , where i runs from 1 to the number of input dimensions, m.

### Weights 
w ij , are the weighted connections between nodes i and j. For neural networks
these weights are analogous to the synapses in the brain. They are arranged into a
matrix **W**.

### Outputs 
The output vector is **y**, with elements y j , where j runs from 1 to the number
of output dimensions, n. We can write **y(x, W)** to remind ourselves that the output
depends on the inputs to the algorithm and the current set of weights of the network.

### Targets
The target vector **t**, with elements t j , where j runs from 1 to the number of
output dimensions, n, are the extra data that we need for supervised learning, since
they provide the ‘correct’ answers that the algorithm is learning about.

### Activation Function 
For neural networks, g(·) is a mathematical function that describes
the firing of the neuron as a response to the weighted inputs, such as the threshold
function described in Section 3.1.2.

### Error
E, a function that computes the inaccuracies of the network as a function of the
outputs y and targets **t**.







