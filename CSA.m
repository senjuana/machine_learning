function sBest=CSA(nf)

%OBTENCION DE LIMITE INFERIOR, SUPERIOR, DIMENSION Y FUNCION OBJETIVO
[lb,ub,dim,fobj] = Get_Functions_details(nf);

%% PARAMETROS DEL ALGORITMO
PD=4;   % DIMENSION DEL ESPACIO
N=40;    % NUMERO DE CUERVOS
AP=0.1; % PROBABILIDAD
fl=2;   % FLIGHT LENGHT
it=100; % ITERACIONES

%% ALGORITMO

tic
figure(1)

%INICIALIZACION DE LAS VARIABLES
for i=1:N

    for j=1:PD
        
        for var=1:dim
            xi(var,i,j)=lb+rand*(ub-lb);
        end
        s(i,j)=fobj(xi(:,i,j));
        
    end

end

%MEMORIAS (COMIDA GUARDADA DE CALIDAD)
memx=xi;
mems=s;

%ITERACIONES
for t=1:it

    %POR CADA CUERVO
    for i=1:N

        %CUERVO A SEGUIR
        follow=round(N*rand);
        while(follow==0)
            follow=round(N*rand);
        end

        %PROBABILIDAD DE SEGUIR A OTRO CUERVO
        if rand>AP
            xi(:,i,:)= xi(:,i,:)+fl*rand*(memx(:,follow,:)-xi(:,i,:));
            
            for j=1:PD
                for var=1:dim
             if(xi(var,i,j)<lb || xi(var,i,j)>ub)
                 xi(var,i,j)= lb+rand*(ub-lb); 
             end
                end
            end
            
        else

            for j=1:PD
                for var=1:dim
                    xi(var,i,j)=lb+rand*(ub-lb);
                end
            end

        end
    end

    %EVALUACION DE NUEVAS SOLUCIONES
    for i=1:N

        for j=1:PD
            s(i,j)=fobj(xi(:,i,j));
        end

    end

    %GUARDADO DE MEJOR COMIDA
    for i=1:N

        for k=1:PD

            if s(i,k)<mems(i,k)
                memx(:,i,k)=xi(:,i,k);
                mems(i,k)=s(i,k);
            end

        end

    end

     %GRAFICA DE RESPUESTA EN TIEMPO
       optim(t)=min(min(mems));
       
end
 hold on
 plot(optim,'g')
 xlabel('iteration');
 ylabel('optimum');

%BUSQUEDA EN MEMORIA DEL VALOR OPTIMO
sBest=inf;
for i=1:N

   [optn indn]=min(mems(i,:));
   if(optn<sBest)
      sBest=optn;
      xBest=memx(:,i,indn);
   end

end

%% RESULTADOS
disp(' ')
disp('---RESULTADOS CROW SEARCH ALGORITHM---')
fprintf('MINIMO GLOBAL EN EL RANGO: %f \n',sBest)
fprintf('VALOR DE LAS VARIABLES')
disp(xBest)
toc

end
