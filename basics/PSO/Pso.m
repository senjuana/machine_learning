clc;
clear;
close all; 

%% definicion del problema 
CostFunction = @(x) Sphere(x);  % cost function

nVar = 2; % valor del espacio dimensional

VarSize = [1 nVar]; % espacio dimensional

VarMin = -10; % minimo
VarMax =  10; % maximo


%% Parametros de PSO

MaxIt = 500; % maximas iteraciones

nPop = 50; % tama�o de la poblacion de particulas

w = 1;  % coeficiente inercial 
wdamp = 0.99; % Damping ratio of Inertia coefficent  
c1 = 2; % coeficiente personal de aceleracion
c2 = 2; % coeficiente social de aceleracion


%% inicializacion 

% template de particula
empty_particle.Position = [];
empty_particle.Velocity = [];
empty_particle.Cost = [];
empty_particle.Best.Position = [];
emty_particle.Best.Cost = [];

%creacion del array de poblacion
particle = repmat(empty_particle, nPop,1);

% inicializacion  del Goblal best
GlobalBest.Cost = inf;

%inilizacion de la poblacion
for i=1:nPop
    %genera una solucion random
    particle(i).Position = unifrnd(VarMin,VarMax,VarSize);
    
    %inicializacion de la velocidad
    particle(i).Velocity = zeros(VarSize);
    
    %evaluacion 
    particle(i).Cost = CostFunction(particle(i).Position);
    
    % update del mejor personal 
    particle(i).Best.Position = particle(i).Position;
    particle(i).Best.Cost = particle(i).Cost;
    
    %update Global Best 
    if particle(i).Best.Cost < GlobalBest.Cost
        GlobalBest = particle(i).Best;
    end
    
end

% array para mantener el mejor costo en cada iteracion 
BestCosts = zeros(MaxIt,1);


%% main

for it=1:MaxIt
   
   for i=1:nPop
       %Update Velocity
       particle(i).velocity =  w *  particle(i).Velocity ...
           + c1 * rand(VarSize).*(particle(i).Best.Position - particle(i).Position)
           + c2 * rand(VarSize).*(GlobalBest.Position - particle(i).Position);
       
       %Update Position
       particle(i).Position = particle(i).Position + particle(i).velocity;
       %Evaluacion 
       particle(i).Cost = CostFunction(particle(i).Position);
       
       %Update personal best
       if particle(i).Cost < particle(i).Best.Cost
           
          particle(i).Best.Position = particle(i).Position;
          particle(i).Best.Cost = particle(i).Cost;
          
          %update Global Best 
          if particle(i).Best.Cost < GlobalBest.Cost
              GlobalBest = particle(i).Best;
          end
       end
   end
   
   
   % Store the Best Cost Value
   BestCosts(it) = GlobalBest.Cost;
   
   %Display iteration Information
   %disp(['Iteration' num2str(it) ': Best Cost  = ' num2str(BestCosts(it))] );
   
   %Damping inertia Coefficient
   w = w * wdamp;
end


%% Resultados

disp(GlobalBest);


