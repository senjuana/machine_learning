%forrester function fo 2 dimensions
function fx = forrester(x)

fact1 = (6*x - 2)^2;
fact2 = sin(12*x - 4);

fx = fact1 * fact2;
