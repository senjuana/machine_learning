%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%
%PSO Est�ndar, propuesto por Kennedy y Eberhart [31]
%Prueba con funci�n de Rosenbrock
%Erik Cuevas, Valent�n Osuna-Enciso, Diego Oliva, Margarita D�az
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%
%Limpiar memoria y cerrar ventanas de MatLAB
close all
clear all
%dimensiones del espacio de b�squeda
d = 2;
%l�mites del espacio de b�squeda
%l�mite inferior
l = [-5, -5];
%l�mite superior
u = [10, 10];
%M�ximo n�mero te iteraciones de PSO
Max_iter = 500;
%Cantidad de part�culas en la poblaci�n
Part_N = 50;

%Proceso de inicializaci�n de PSO Ecuaci�n 4.1
x = l(1) + rand(Part_N,d).* (u(1) - l(1));
%Eval�a la funci�n objetivo
for i = 1:Part_N
obj_func(i,:) = rosenbrock(x(i,:));
end

%Obtiene el mejor valor global (m�nimo)
[glob_opt, ind] = min(obj_func);
%Vector de �ptimo global
%Vector de unos del tama�o de la poblaci�n
G_opt = ones(Part_N,d);
%Valores del �ptimo en dimensi�n 1
G_opt(:,1) = x(ind,1);
%Valores del �ptimo en dimensi�n 2
G_opt(:,2) = x(ind,2);
Mejor_pos = [x(ind,1),x(ind,2)];
%Mejor local para cada part�cula
Loc_opt = x;
%Inicializa velocidades
v = zeros(Part_N,d);
%inicia proceso de optimizaci�n PSO
t = 1;
%Criterio de paro
while t < Max_iter
%Calcula la nueva velocidad ecuaci�n 4.2
v = v + rand(Part_N,d) .* (Loc_opt - x) + rand(Part_N,d) .* (G_opt - x);
%Calcula nueva posici�n ecuaci�n 4.3
x = x + v;
for i = 1:Part_N
%Se verifica que las part�culas no se salgan de los l�mites u y l
if x(i,1) > u(1)
x(i,1) = u(1);
elseif x(i,1) < l(1)
x(i,1) = l(1);
elseif x(i,2) > u(2)
x(i,2) = u(2);
elseif x(i,2) < l(2)
x(i,2) = l(2);
end
%Se eval�an las nuevas posiciones en la funci�n objetivo
Nva_obj_func(i,:) = rosenbrock(x(i,:));
%Se verifica si se actualizan los �ptimos locales
if Nva_obj_func(i,:) < obj_func(i,:)
%Actualiza �ptimo local
Loc_opt(i,:) = x(i,:);
%Actualiza funci�n objetivo
obj_func(i,:) = Nva_obj_func(i,:);
end
end
%Obtiene el mejor valor global (m�nimo)
[Nvo_glob_opt, ind] = min(obj_func);
%Se verifica si se actualiza el �ptimo global
if Nvo_glob_opt < glob_opt
glob_opt = Nvo_glob_opt;
%Valores del �ptimo en dimensi�n 1
G_opt(:,1) = x(ind,1);
%Valores del �ptimo en dimensi�n 2
G_opt(:,2) = x(ind,2);
Mejor_pos = [x(ind,1),x(ind,2)];
end
%Almacena los valores de funci�n objetivo en cada iteraci�n
Evol_func_obj(t) = glob_opt;
%Incrementa iteraciones
t = t + 1;
end
%Grafica la evoluci�n de la funci�n objetivo
plot(Evol_func_obj)
disp(['Mejor posici�n x: ',num2str(Mejor_pos)])
disp(['Mejor valor funci�n objetivo: ',num2str(glob_opt)])