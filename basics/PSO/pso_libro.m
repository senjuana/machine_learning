close all 
clear all 
%dimensiones del search space
d = 2;
%limites del search space
l=[-5,-5]; % limite inferior
u=[10,10]; % limite superior 
maxIter = 500;
particulas = 50;
%data =[];

%% inicializacion 
x = l(1) + rand(particulas,d).* (u(1) -l(1)); % ya lo entiendo

%evaluacion de la funcion 
for i = 1:particulas
    obj_func(i,:) = ackley([i,i],40,0.2,2*pi);
    %data(i) = forrester(i);
end
plot(obj_func)
%obtine el mejor valor global(minimo)
[glob_opt,ind] = min(obj_func);
%Vector de optimo global
%vector de tama�os de la poblacion 
G_opt = ones(particulas,d);
%valores optimos de las dimensiones
G_opt(:,1) = x(ind,1);
G_opt(:,2) = x(ind,2);
Mejor_pos = [x(ind,1),x(ind,2)];
%mejor local para cada particula 
Loc_opt = x;
%init de velocidades
v = zeros(particulas,d);

%% PSO

t = 1;
while t < maxIter
   v = v + rand(particulas,d) .* (Loc_opt - x) + rand(particulas,d) .* (G_opt - x);
   x = x + v; 
   
   for i = 1:particulas
       % validacion de las particulas en el search space
       if x(i,1) > u(1)
           x(i,1) = u(1);
       elseif x(i,1) <l(1)
           x(i,1) = l(1);
       elseif x(i,2) > u(2)
           x(i,2) = u(2);
       elseif x(i,2) < u(2)
           x(i,2) = l(2);
       end
       
       %se evaluan  las nuevas posiciones en la funcion objetivo 
       Nva_obj_func(i,:) = ackley([i,i],40,0.2,2*pi);
       if Nva_obj_func(i,:) > obj_func(i,:);
           %update optimo local
           Loc_opt(i,:) = x(i,:);
           %Actualiza funcion objetivo
           obj_func(i,:) = Nva_obj_func(i,:);
       end
   end
   %obtencion del mejor valor global (minimo)
   [Nvo_glob_opt,ind] = max(obj_func);
   %Se verifica si se actualiza el �ptimo global
   if Nvo_glob_opt > glob_opt
       glob_opt = Nvo_glob_opt;
       %Valores del �ptimo en dimensi�n 1
       G_opt(:,1) = x(ind,1);
       %Valores del �ptimo en dimensi�n 2
       G_opt(:,2) = x(ind,2);
       Mejor_pos = [x(ind,1),x(ind,2)];
       
   end
   %Almacena los valores de funci�n objetivo en cada iteraci�n
   Evol_func_obj(t) = glob_opt;
   %disp(['Mejor valor funcion objetivo: ',num2str(glob_opt)])
   %Incrementa iteraciones
   t = t + 1;
end


%% Grafica la evoluci�n de la funci�n objetivo
%plot(Evol_func_obj)
%disp(['Mejor posicion x: ',num2str(Mejor_pos)])
disp(['Mejor valor funcion objetivo: ',num2str(glob_opt)])



