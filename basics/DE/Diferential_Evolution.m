%% Inicializacion parametros

Np = 40;
D = 2;   %dimensiones
Cr = 0.9;%probabilidad de mutacion 
F = 0.5; %operador de cruzamiento

% vectores
vectorV = zeros(Np,D);
vectorU = zeros(Np,D);

% init vector
for i=1:Np
    for j=1:D
        vectorV(i,j) = randi([-20,20]);
    end
end

NumEvaluaciones = 0;

figure(1);

%% algoritmo
while NumEvaluaciones < 200
    for i=1:Np
        r0 = i;
        while r0 == i
           r0 = floor(randi(40));
        end
        r1 = r0;
        while r1 == r0 || r1 == i
            r1 = floor( randi(40));
        end
		r2 = r1;
        while r2 == r1 || r2 == r0 || r2 == i
            r2 = floor(randi(20) * D);
        end
        jrand = floor(randi(100) * D);
        
        for j=1:D
           if randi(100) <= Cr || j == jrand
               vectorU(i,j) = vectorV(r0,j) + F * (vectorV(r1,j) - vectorV(r2,j));
		   else
               vectorU(i,j) = vectorV(i,j);
           end
        end 
    end
    
    for k=1:Np
        if rosenbrock(vectorU(k,1), vectorU(k,2)) < rosenbrock(vectorV(r1,j),vectorV(k,2))
           vectorV(k,1) = vectorU(k,1); 
           vectorV(k,2) = vectorU(k,2); 
        end
    end
    
   plot(vectorU(:,1),vectorU(:,2),'*g');
   hold on;
   plot(vectorV(:,1),vectorV(:,2),'.r');
   pause(0.005);
    
    NumEvaluaciones = NumEvaluaciones + 1;
end




%% resultados
%plot(vectorV(:,1),vectorV(:,2),'.');
disp(vectorV);
