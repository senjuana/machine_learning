function [sol, obj, dd] = hormigas(Ncity, maxit, a, b ,rr, Q)
%Ncity: Número de ciudades de recorrido
% maxit: Número maximo de iteraciones
%a=0: Se selecciona la ciudad mas cercana 
%b=0: Tl el algoritmo solo funciona ocm feromonas y no con distancias de la ciudades
% rr: Tasa de evaporacion del rastro de feromonas en el camino 
% Q: Seleciona el tamaño de camino optimo 

% sol: Solución de problema de TSP
% obj: Distancia del camino optimo, funcion objetivo
% dd: dbest y dmin funciones en cada iteracion

global dcity  % dcity: matriz de distancia entre ciudades
dbest = 9999999;
e = 5; % paramentro que pondera la importancia del rastro de feromona del mejor camino
Nants = Ncity; % Número de hormigas 
vis= 1./dcity; % heuristica (igual a la inversa de la distancia)
phmone=.1*ones(Ncity, Ncity); %inicia las feromonas entre las ciudades

% Inicia el camino
for ic = 1 : Nants
	tour(ic ,:) = randperm(Ncity);
end
tour(:, Ncity+1) = tour(:, 1);% Cierra el camino

for it=1: maxit
    % encuentra el recorrido de las ciudades de cada hormiga
    %st es la ciudad actual
    %nxt contiene el resto de ciudades todavia no visitadas
	for ia=1: Nants
		for iq=2:Ncity-1
			st=tour(ia, iq-1); % Ciudad actual
			nxt=tour(ia, iq:Ncity);%Ciudades que van despues de acutal
            %Probabilidad de transicion 
			prob=((phmone(st, nxt).^a).*(vis(st, nxt).^b))./sum((phmone(st, nxt).^a).*(vis(st, nxt).^b));
			rcity=rand;
			for iz=1:length(prob)
				if rcity<sum(prob(1:iz))
					newcity=iq-1+iz; % siguiente ciudad a visitar
					breack
                end
			end
			temp = tour(ia, newcity); % Pone la nueva ciudad
			% Seleciona el siguiente camino
            tour(ia, newcity) = tour(ia, iq);
			tour(ia, iq) = temp;
		end
    end
    
 %Calcula la longitud de cada recorrido y distribución de feromonas
 
phtemp=zeros(Ncity, Ncity);
for ic=1:Nants
    dist(ic,1)=0;
    for id=1:Ncity
        dist(ic,1)=dist(ic)+dcity(tour(ic, id), tour(ic,id+1));
        phtemp(tour(ic,id),tour(ic,id+1))=phtemp(tour(ic,id));
            tour(ic,id+1)+Q/dist(ic,1);
           
    end
end

% Actualizamos la mejor solución
    [dmin, ind]=min(dist);
        if dmin<dbest
            dbest=dmin;
            sol=tour(ind,:);
        end
% calculo de la feromona del mejor camino recorrido por las hormigas

        ph1=zeros(Ncity, Ncity);
            for id=1:Ncity
                ph1(tour(ind, id), tour(ind, id+1))=Q/dbest;
            end
% actualización de la feromona 
        phmone=(1-rr)*phmone+phtemp+e*ph1;
        dd(id,:)=[dbest dmin];
        [it dmin dbest]
        end


obj=dbest; % solución optima obtenida por el algoritmo

end 