%%Inicio de parametros
data = [];
for i=1:50
    data(i) = forrester(i); 
end

%data = rosenbrock(50);
figure(1)
plot(data)
grid on

%% algoritmo

Best =  randi([1 50],1,1); %mejor inicial 
%Best = 40;
disp(['Mejor Inicial: ' num2str(Best)]);

token = 1; % token de estancamiento
Aux = 0;
iter = 1;

while  token ~= 3

    iter = iter + 1;
    % creacion de los vecinos
    R = Best-1; 
    L = Best+1;
 
    % validacion de search space
    if R < 1
       token = 3; 
    end
    if L > 50
       token = 3; 
    end
    %update de el mejor
    if forrester(Best) < forrester(R)
        Best = R;
    end
    if forrester(Best) < forrester(L)
        Best = L;
    end
    %validacion del estancamiento
    if Best == Aux
       token = token + 1; 
    end
     Aux = Best;
end

%% Resultado
disp(['terminado en ' num2str(iter) ' iteraciones.']);
disp(['Cordenadas del mejor local: ' num2str(Best) ' y: ' num2str(forrester(Best))]);





