%% Inicializacion

%Limpiar memoria y cerrar ventanas de MatLAB
close all
clear all

%dimensiones del espacio de busqueda
d = 2;

%limites del espacio de busqueda
%limite inferior
l = [-5, -5];
%limite superior
u = [10, 10];
%Maximo numero te iteraciones de PSO
Max_iter = 150;
%Cantidad de particulas en la poblacion
Part_N = 50;
% valores de coeficientes
w = 1;  % coeficiente inercial 
wdamp = 0.99; % Damping ratio of Inertia coefficent  
c1 = 0; % coeficiente personal de aceleracion
c2 = 2; % coeficiente social de aceleracion

%Proceso de inicializacion de PSO Ecuacion 4.1
x = l(1) + rand(Part_N,d).* (u(1) - l(1));

%Evalua la funcion objetivo
for i = 1:Part_N
obj_func(i,:) = rosenbrock(x(i,:));
end
plot(obj_func)
%Obtiene el mejor valor global (minimo)
[glob_opt, ind] = min(obj_func);
%Vector de optimo global
%Vector de unos del tamaño de la poblacion
G_opt = ones(Part_N,d);
%Valores del optimo en dimension 1
G_opt(:,1) = x(ind,1);
%Valores del optimo en dimension 2
G_opt(:,2) = x(ind,2);
Mejor_pos = [x(ind,1),x(ind,2)];
%Mejor local para cada particula
Loc_opt = x;
%Inicializa velocidades
v = zeros(Part_N,d);
%inicia proceso de optimizacion PSO
t = 1;


%% Criterio de paro

while t < Max_iter
% generacion de las antiparticulas 
y = x;
for i = 1:Part_N
   y(i,1) = (10 + (-5)) - y(i,1); 
   y(i,2) = (10 + (-5)) - y(i,2);
end

% revision de las mejores particulas
for i = 1:Part_N
    if rosenbrock(y(i,:)) < rosenbrock(x(i,:))
        x(i,1) = y(i,1);
        x(i,2) = y(i,2);
    end
end

% ploteo
figure(1);
subplot(2,2,1),plot(x(:,1),x(:,2),'*g')
xlim([-6,6]);
ylim([-10,10]);
xlabel('eje x');
ylabel('eje y');
title('OBPSO');
pause(0.01);



%Calcula la nueva velocidad ecuacion 4.2
v = w * v + c1 * rand(Part_N,d) .* (Loc_opt - x) + c2 * rand(Part_N,d) .* (G_opt - x);
%Calcula nueva posicion ecuacion 4.3
x = x + v;

for i = 1:Part_N
    %Se verifica que las particulas no se salgan de los limites u y l
    if x(i,1) > u(1)
        x(i,1) = u(1);
    elseif x(i,1) < l(1)
        x(i,1) = l(1);
    elseif x(i,2) > u(2)
        x(i,2) = u(2);
    elseif x(i,2) < l(2)
        x(i,2) = l(2);
    end

    %Se evaluan las nuevas posiciones en la funcion objetivo
    Nva_obj_func(i,:) = rosenbrock(x(i,:));
    %Se verifica si se actualizan los optimos locales
    if Nva_obj_func(i,:) < obj_func(i,:)
        %Actualiza optimo local
        Loc_opt(i,:) = x(i,:);
        %Actualiza funcion objetivo
        obj_func(i,:) = Nva_obj_func(i,:);
    end
end
%Obtiene el mejor valor global (minimo)
[Nvo_glob_opt, ind] = min(obj_func);
%Se verifica si se actualiza el optimo global
if Nvo_glob_opt < glob_opt
    glob_opt = Nvo_glob_opt;
    %Valores del optimo en dimension 1
    G_opt(:,1) = x(ind,1);
    %Valores del optimo en dimension 2
    G_opt(:,2) = x(ind,2);
    Mejor_pos = [x(ind,1),x(ind,2)];
end
%Almacena los valores de funcion objetivo en cada iteracion
Evol_func_obj(t) = glob_opt;
%Incrementa iteraciones
t = t + 1;

%Damping inertia Coefficient
w = w * wdamp;
end


%% Grafica la evolucion de la funcion objetivo
subplot(2,2,2),plot(Evol_func_obj)
xlabel('iteraciones');
ylabel('global best');

clear all
%disp(['Mejor posicion x: ',num2str(Mejor_pos)])
%disp(['Mejor valor funcion objetivo: ',num2str(glob_opt)])




 

%% pso original

d = 2;
%limites del espacio de b�squeda
%limite inferior
l = [-5, -5];
%limite superior
u = [10, 10];
%Maximo numero te iteraciones de PSO
Max_iter = 150;
%Cantidad de particulas en la poblacion
Part_N = 50;
% valores de coeficientes
w = 1;  % coeficiente inercial 
wdamp = 0.99; % Damping ratio of Inertia coefficent  
c1 = 0; % coeficiente personal de aceleracion
c2 = 2; % coeficiente social de aceleracion

%Proceso de inicializacion de PSO Ecuacion 4.1
x = l(1) + rand(Part_N,d).* (u(1) - l(1));

%Evalua la funcion objetivo
for i = 1:Part_N
obj_func(i,:) = rosenbrock(x(i,:));
end

%Obtiene el mejor valor global (minimo)
[glob_opt, ind] = min(obj_func);
%Vector de optimo global
%Vector de unos del tamaño de la poblacion
G_opt = ones(Part_N,d);
%Valores del optimo en dimension 1
G_opt(:,1) = x(ind,1);
%Valores del optimo en dimension 2
G_opt(:,2) = x(ind,2);
Mejor_pos = [x(ind,1),x(ind,2)];
%Mejor local para cada particula
Loc_opt = x;
%Inicializa velocidades
v = zeros(Part_N,d);
%inicia proceso de optimizacion PSO
t = 1;


%% Criterio de paro

while t < Max_iter
% ploteo
figure(1);
subplot(2,2,3),plot(x(:,1),x(:,2),'*r')
xlim([-6,6]);
ylim([-10,10]);
xlabel('eje x');
ylabel('eje y');
title('PSO');
pause(0.01);


%Calcula la nueva velocidad ecuacion 4.2
v = w * v + c1 * rand(Part_N,d) .* (Loc_opt - x) + c2 * rand(Part_N,d) .* (G_opt - x);
%Calcula nueva posicion ecuacion 4.3
x = x + v;

for i = 1:Part_N
    %Se verifica que las particulas no se salgan de los limites u y l
    if x(i,1) > u(1)
        x(i,1) = u(1);
    elseif x(i,1) < l(1)
        x(i,1) = l(1);
    elseif x(i,2) > u(2)
        x(i,2) = u(2);
    elseif x(i,2) < l(2)
        x(i,2) = l(2);
    end

    %Se evaloan las nuevas posiciones en la funcion objetivo
    Nva_obj_func(i,:) = rosenbrock(x(i,:));
    %Se verifica si se actualizan los optimos locales
    if Nva_obj_func(i,:) < obj_func(i,:)
        %Actualiza optimo local
        Loc_opt(i,:) = x(i,:);
        %Actualiza funcion objetivo
        obj_func(i,:) = Nva_obj_func(i,:);
    end
end
%Obtiene el mejor valor global (monimo)
[Nvo_glob_opt, ind] = min(obj_func);
%Se verifica si se actualiza el optimo global
if Nvo_glob_opt < glob_opt
    glob_opt = Nvo_glob_opt;
    %Valores del optimo en dimension 1
    G_opt(:,1) = x(ind,1);
    %Valores del optimo en dimension 2
    G_opt(:,2) = x(ind,2);
    Mejor_pos = [x(ind,1),x(ind,2)];
end
%Almacena los valores de funcion objetivo en cada iteraci�n
Evol_func_obj(t) = glob_opt;
%Incrementa iteraciones
t = t + 1;

%Damping inertia Coefficient
w = w * wdamp;
end


%% Grafica la evolucion de la funcion objetivo

subplot(2,2,4),plot(Evol_func_obj)
xlabel('iteraciones');
ylabel('global best');
disp(['Mejor posicion x: ',num2str(Mejor_pos)])
disp(['Mejor valor funcion objetivo: ',num2str(glob_opt)])

