%% Inicializacion de parametros
%limpieaza de variables y entorno 
close all 
clear all 

%numero de dimensiones
d = 2; 
%limites del espacio de busqueda
u = [10,10];
l = [-5,-5];

Bw= [100,100];
Hms =100;
Hcmr= 0.75;
Par = 0.5;

Ni =1500;
 %inicializacion de la memoria de armonias
 x = l(1) + rand(Hms,d).*(u(1) - l(1));
 
%Evalua la funcion objetivo
for i = 1:Hms
fx(i,:) = rosenbrock(x(i,:));
end

%genera la memoria de la harmonia ordenando la funcion 
[fxtemp,index] = sort(fx,'ascend'); %minimizar
Hm = x(index,:);
Hmfx = fx(index); %valores de la funcion objetivo

%% Algoritmo HS
for it = 1:Ni
	%operadores 
	for j = 1:d
		%usando Hcmr  se decide la siguiente harmonia
		if (rand >= Hcmr)
			xHs(j) = l(j) + rand .* (u(j)- l(j));
		else
			xHs(j) = Hm(fix(Hms * rand ) + 1,j);
		if (rand <= Par)
			pa = (u(j)-l(j))/Bw(j);
			xHs(j) = xHs(j) + pa * (rand-0.5);
		end
		end
	end

	%se evalua la nueva armonia
	fbest = rosenbrock(xHs);
	% se toma el peor valor de Hm
	fHspeor = Hmfx(Hms);
	%se compara el valor de la harmonia con la peor 
	if fbest < fHspeor
		Hm(Hms,:) = xHs;
		Hmfx(Hms) = fbest;
	end

	%se ordena el Hm deacuerdo a su valor en la funcion
	[fxtemp,index] = sort(Hmfx,'ascend');
	Hm = Hm(index,:);

	%evalua Hm en la funcion
	for i =1:Hms
		Hmfx(i,:) = rosenbrock(Hm(i,:));
	end
	%almacena el mejor para esta iteracion
	Evol_func_obj(it) = Hmfx(1);

end


%% Resultados
plot(Evol_func_obj);
disp(['Mejor posicion de x: ',num2str(Hm(1,:))]);
disp(['Mejor valor en la funcion: ', num2str(Hmfx(1))])


