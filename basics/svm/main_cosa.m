% clean the workspace 
clear ; close all; clc

%% initialization of parameters

[X,Y,Nclass]=SelectDataSet('iris');

% Values from the paper
VarMin.y=0.01;    % Lower Bound of sigma parameter
VarMax.y=100;     % Upper Bound of sigma parameter

kernel='gaussian';
verbose=0;
lambda=1e-2;


%% PARAMETROS DEL ALGORITMO

PD=1;        % DIMENSION DEL ESPACIO
N=4;         % NUMERO DE BUHOS
B=1.9;       % CONSTANTE
it=20;       % ITERACIONES
Bi=B;
dim=30;       %NUMERO DE BUHOS

% paper values
ub=3500; %valor alto de C
lb=0.01; % valor bajo e C

% INICIALIZACION DE VARIABLE CAOTICA
z=rand;
while(z==0.25 || z==0.5 || z==0.75 || z==1)
    z=rand;
end


%% algorithm
runs=100;
for R=1:runs 
    clear  Temp1 Temp2 xsup ypred Test TestL TrL sig c s xi Optimum
    
    %init buhos
    for i=1:N
        for j=1:PD
            for var=1:dim
                xi(var,i,j)=lb+rand*(ub-lb);
            end
        end
    end 
    
    s=randi(100);
    sig=randi(10);
    c = 3500;
    Optimum=inf; %INICIALIZACION DEL OTPIMO (PARA MINIMIZAR, SI SE DESDE MAXIMIZAR DEBERA SER -inf)
    
    
    y=randperm(size(X,1));
    Temp=ceil(size(X,1)*1/2);

    % Dividing the data into training and testing sets
    Tr=X(y(1,1:Temp),:);
    Test=X(y(1,Temp+1:size(X,1)),:);
    TrL=Y(y(1,1:Temp),1);
    TestL=Y(y(1,Temp+1:size(X,1)),1);

    % ITERACIONES DEL ALGORITMO
    for t=1:it
    
        %EVALUACION Y DECLARACION DEL OPTIMO GLOBAL
        for k=1:N
            for l=1:PD
           
                % Evaluation
                [xsup,we,b,nbsv]=svmmulticlassoneagainstall(Tr,TrL,Nclass,s(1,l),lambda,kernel,sig,verbose);
                [ypred] = svmmultival(Test,xsup,we,b,nbsv,kernel,sig);
                TestingError=sum(ypred==TestL)*100/size(TestL,1);
                Fitness(l)=100-TestingError;
                
                s(N)=Fitness(l);  
                
            if (s(N)<Optimum) %CONDICION PARA MINIMIZAR (INVERTIR PARA MAXIMIZAR)
               Optimum=s(N);  %GUARDADO DE MEJOR FITNESS
               for var=1:dim    %GUARDADO DE MEJOR SOLUCION GLOBAL
                   Vx(var)=xi(var,k,l);
               end 
            end   
            
            end 
        end  
    
        % ACTUALIZACION DE LAS POSICIONES DE LOS BUHOS
        for i=1:N
    
        w=min(s(1,:)); %MEJOR MINIMO DEL ESPACIO DE BUSQUEDA DEL BUHO
        b=max(s(1,:)); %MEJOR MAXIMO DEL ESPACIO DE BUSQUEDA DEL BUHO
    
        for j=1:PD
     
            I=(s(1,j)-w)/(b-w); % RESONANCIA 
            
            %CALCULO DE DISTANCIA E INTENSIDAD POR CADA DIMENSION (VARIABLE)
            for var=1:dim
                Rx(var)=(xi(var,i,j)^2) + (Vx(var)^2); %DISTANCIA EUCLIDIANA
                Icx(var)=I/(Rx(var)^2)+z;              %INTENSIDAD
            end
            
            
            %UPDATE DE POSICION
            if (rand<0.5)
                
                for var=1:dim
                    xi(var,i,j) = xi(var,i,j) + Bi*Icx(var)*abs((0.5*rand*Vx(var))-xi(var,i,j)); 
                    
                    if(xi(var,i,j)<lb || xi(var,i,j)>ub) %VERIFICACION DE NO EXCEDER LIMITES
                       xi(var,i,j)= lb+rand*(ub-lb); 
                    end
                    
                end
               
            else
                
                for var=1:dim
                    xi(var,i,j) = xi(var,i,j) - Bi*Icx(var)*abs((0.5*rand*Vx(var))-xi(var,i,j));
                    
                    if(xi(var,i,j)<lb || xi(var,i,j)>ub) %VERIFICACION DE NO EXCEDER LIMITES
                       xi(var,i,j)= lb+rand*(ub-lb); 
                    end
                    
                end
            
            end
      
           %UPDATE VARIABLE CAOTICA
           
           z = cos(t*acos(z));
           
            
        end
     
        end
    
        %CONSTANTE EN DECRECION LINEAL
        Bi=B-((B/it)*t);
        %disp(['iteration: ' num2str(t)]); 
      
    end
    
    % Output/display
    for i=1:dim
        if c > min(xi(i,:))
            c= min(xi(i,:));
        end
    end
   
    disp(['corrida:',num2str(R) ' Best value of C=',num2str(c) ' Best value of Sigma=' num2str(sig) ' fmin=',num2str(Optimum)]);
    evo(R)=Optimum;
end

figure(1);
plot(evo);

