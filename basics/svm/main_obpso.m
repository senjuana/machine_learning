% clean the workspace 
clear ; close all; clc

%% initialization of parameters

tic

[X,Y,Nclass]=SelectDataSet('iris');

% Values from the paper
VarMin.x=0.01;           % Lower Bound of C parameter
VarMax.x=3500;           % Upper Bound of C parameter
VarMin.y=0.01;           % Lower Bound of sigma parameter
VarMax.y=100;           % Upper Bound of sigma parameter

alpha=0.1;
VelMax.x=alpha*(VarMax.x-VarMin.x);    % Maximum Velocity (C)
VelMin.x=-VelMax.x;                    % Minimum Velocity (C)
VelMax.y=alpha*(VarMax.y-VarMin.y);    % Maximum Velocity (Sigma)
VelMin.y=-VelMax.y;                    % Minimum Velocity (Sigma)

%parametros PSO
 %% Parameter initialization
PSn=5;
obpso_size = PSn;                       % number of the obpso particles
maxIter = 40;                          % maximum number of iterations
w=1;                % Inertia Weight
wdamp=0.98;         % Inertia Weight Damping Ratio
correction_factor1 = 2;
correction_factor2 = 2;
kernel='gaussian';
verbose=0;
lambda=1e-2;
GlobalBest.Cost=inf;


%% algorithm

runs=1;
for R=1:runs 
    clear obpso Temp1 Temp2 xsup ypred Test TestL TrL
    y=randperm(size(X,1));
    Temp=ceil(size(X,1)*1/2);

    % Dividing the data into training and testing sets
    Tr=X(y(1,1:Temp),:);
    Test=X(y(1,Temp+1:size(X,1)),:);
    TrL=Y(y(1,1:Temp),1);
    TestL=Y(y(1,Temp+1:size(X,1)),1);

    

    % Initialize the population/solutions
    for i=1:PSn
        obpso(i,1,1)=randi(100); %  Penalty parameter (C)
        obpso(i,1,2)=randi(10); %  RBF Kernel (Sigma)
        
        obpso(:,2,:) = 0;   % set initial velocity for particles
        obpso(:,4,1) = 100; % set initial best solution for particles
        
        % Evaluation
        [xsup,we,b,nbsv]=svmmulticlassoneagainstall(Tr,TrL,Nclass,obpso(i, 1, 1),lambda,kernel,obpso(i, 1, 2),verbose);
        [ypred] = svmmultival(Test,xsup,we,b,nbsv,kernel,obpso(i, 1, 2)); 
        TestingError=sum(ypred==TestL)*100/size(TestL,1);
        Fitness(i)=100-TestingError;
    
        if Fitness(i) < obpso(i,4,1)
            obpso(i, 3, 1) = obpso(i, 1, 1);                  % update best x position,
            obpso(i, 3, 2) = obpso(i, 1, 2);                  % update best x position,
            obpso(i, 4, 1) = Fitness(i);                       % update the best value so far
        end
        if Fitness(i)<GlobalBest.Cost        
            GlobalBest.Cost=Fitness(i);
            GlobalBest.x=obpso(i, 1, 1); 
            GlobalBest.y=obpso(i, 1, 2);             
        end
    end
    
    for iter = 1:maxIter
        
        for i=1:PSn
            %Update Velocity
            obpso(i, 2, 1) = rand*w*obpso(i, 2, 1) + correction_factor1*rand*(obpso(i, 3, 1) - obpso(i, 1, 1)) + correction_factor2*rand*(GlobalBest.x - obpso(i, 1, 1));   %x velocity component
            obpso(i, 2, 2) = rand*w*obpso(i, 2, 2) + correction_factor1*rand*(obpso(i, 3, 2) - obpso(i, 1, 2)) + correction_factor2*rand*(GlobalBest.y - obpso(i, 1, 2));   %y velocity component
            
            % Update Velocity Bounds
            obpso(i, 2, 1) = max(obpso(i, 2, 1),VelMin.x);
            obpso(i, 2, 1) = min(obpso(i, 2, 2),VelMax.x);
        
            obpso(i, 2, 2) = max(obpso(i, 2, 2),VelMin.y);
            obpso(i, 2, 2) = min(obpso(i, 2, 2),VelMax.y);
            
            % Update Position
            obpso(i, 1, 1) = obpso(i, 1, 1) + obpso(i, 2, 1);
            obpso(i, 1, 2) = obpso(i, 1, 2) + obpso(i, 2, 2);
            
            % Update Position Bounds
            obpso(i, 1, 1) = max(obpso(i, 1, 1),VarMin.x);
            obpso(i, 1, 1) = min(obpso(i, 1, 1),VarMax.x);
            obpso(i, 1, 2) = max(obpso(i, 1, 2),VarMin.y);
            obpso(i, 1, 2) = min(obpso(i, 1, 2),VarMax.y);
    
            % Evaluation
            [xsup,we,b,nbsv]=svmmulticlassoneagainstall(Tr,TrL,Nclass,obpso(i, 1, 1),lambda,kernel,obpso(i, 1, 2),verbose);
            [ypred] = svmmultival(Test,xsup,we,b,nbsv,kernel,obpso(i, 1, 2)); 
            TestingError=sum(ypred==TestL)*100/size(TestL,1);
            Fitness(i)=100-TestingError;
            
            if Fitness(i) < obpso(i,4,1)
                obpso(i, 3, 1) = obpso(i, 1, 1);                  % update best x position,
                obpso(i, 3, 2) = obpso(i, 1, 2);                  % update best x position,
                obpso(i, 4, 1) = Fitness(i);                       % update the best value so far
            end
            
            if Fitness(i)<GlobalBest.Cost
                GlobalBest.Cost=Fitness(i);
                GlobalBest.x=obpso(i, 1, 1); 
                GlobalBest.y=obpso(i, 1, 2);             
            end
        end
        
        % Store the best solutions for the current run
        best(iter,1)=GlobalBest.x;
        best(iter,2)=GlobalBest.y;
        Gbest(iter,1)=GlobalBest.Cost;       
        
       disp(['iteration: ' num2str(iter)]); 
        
        % Inertia Weight Damping
        w=w*wdamp;
    end

end

%% plot results

    
 % Output/display
    [fmin,ind]=min(Gbest);
    Best=best(ind,:);
    disp(['Best value of C=',num2str(GlobalBest.x) ' Best value of Sigma=' num2str(GlobalBest.y) ' fmin=',num2str(GlobalBest.Cost)]);






