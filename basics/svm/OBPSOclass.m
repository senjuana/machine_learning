%% Oposition based Particle Swarm Optimization
classdef OBPSOclass < handle
	properties
		d = 2 %dimensiones del espacio de busqueda
		l = [-5,-5] %limite inferior
		u =[10,10] %limite superior
		Max_iter = 200
		Part_N = 50%Numero de particulas
		w =1  % coeficiente inercial 
		wdamp = 0.99 % Damping ratio of Inertia coefficent  
		c1 =0% coeficiente personal de aceleracion
		c2 =2% coeficiente social de aceleracion
		x %particulas
		y %antiparticulas
		obj_func %funcion objetivo
		glob_opt 
		ind
		Mejor_pos
		Loc_opt
        G_opt
		Evol_func_obj
		Nva_obj_func
		Nvo_glob_opt
		v %array velocidades
		t = 1%ciclo token
	end
	methods
    %Constructor
    function obj = OBPSOclass()
			obj.x = obj.l(1) + rand(obj.Part_N,obj.d).* (obj.u(1) - obj.l(1));
			for i = 1:obj.Part_N
				obj.obj_func(i,:) = rosenbrock(obj.x(i,:));
			end
			[obj.glob_opt, obj.ind] = min(obj.obj_func);
			obj.G_opt = ones(obj.Part_N,obj.d);
			obj.G_opt(:,1) = obj.x(obj.ind,1);
			obj.G_opt(:,2) = obj.x(obj.ind,2);
			obj.Mejor_pos = [obj.x(obj.ind,1),obj.x(obj.ind,2)];
			obj.Loc_opt = obj.x;
			obj.v = zeros(obj.Part_N,obj.d);
		end

   %metodos de la clase

		function r = algo(obj)
			while obj.t < obj.Max_iter
				% generacion de las antiparticulas 
				obj.y = obj.x;
				for i = 1:obj.Part_N
					obj.y(i,1) = (10 + (-5)) - obj.y(i,1); 
					obj.y(i,2) = (10 + (-5)) - obj.y(i,2);
				end

				% revision de las mejores particulas
				for i = 1:obj.Part_N
					if rosenbrock(obj.y(i,:)) < rosenbrock(obj.x(i,:))
						obj.x(i,1) = obj.y(i,1);
						obj.x(i,2) = obj.y(i,2);
					end
				end


				obj.v = obj.w * obj.v + obj.c1 * rand(obj.Part_N,obj.d) .* (obj.Loc_opt - obj.x) + obj.c2 * rand(obj.Part_N,obj.d) .* (obj.G_opt - obj.x);
				obj.x = obj.x + obj.v;

				for i = 1:obj.Part_N
					%Se evaluan las nuevas posiciones en la funcion objetivo
					obj.Nva_obj_func(i,:) = rosenbrock(obj.x(i,:));
					%Se verifica si se actualizan los optimos locales
					if obj.Nva_obj_func(i,:) < obj.obj_func(i,:)
						%Actualiza optimo local
						obj.Loc_opt(i,:) = obj.x(i,:);
						%Actualiza funcion objetivo
						obj.obj_func(i,:) = obj.Nva_obj_func(i,:);
					end
				end
				%Obtiene el mejor valor global (minimo)
				[obj.Nvo_glob_opt, obj.ind] = min(obj.obj_func);
				%Se verifica si se actualiza el optimo global
				if obj.Nvo_glob_opt < obj.glob_opt
					obj.glob_opt = obj.Nvo_glob_opt;
					%Valores del optimo en dimension 1
					obj.G_opt(:,1) = obj.x(obj.ind,1);
					%Valores del optimo en dimension 2
					obj.G_opt(:,2) = obj.x(obj.ind,2);
					obj.Mejor_pos = [obj.x(obj.ind,1),obj.x(obj.ind,2)];
				end
				%Almacena los valores de funcion objetivo en cada iteracion
				obj.Evol_func_obj(obj.t) = obj.glob_opt;
				%Incrementa iteraciones
				obj.t = obj.t + 1;

				%Damping inertia Coefficient
				obj.w = obj.w * obj.wdamp;
            end
        r =  obj.Evol_func_obj;
		end

	end
end
