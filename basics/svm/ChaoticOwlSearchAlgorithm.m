function Optimum=ChaoticOwlSearchAlgorithm(nf)

%OBTENCION DE LIMITE INFERIOR, SUPERIOR, DIMENSION Y FUNCION OBJETIVO
[lb,ub,dim,fobj] = Get_Functions_details(nf);

%% PARAMETROS DEL ALGORITMO

PD=4;   % DIMENSION DEL ESPACIO
N=40;   % NUMERO DE BUHOS
B=1.9;  % CONSTANTE
it=100; % ITERACIONES
Bi=B;
Optimum=inf; %INICIALIZACION DEL OTPIMO (PARA MINIMIZAR, SI SE DESDE MAXIMIZAR DEBERA SER -inf)

%% ALGORITMO

tic %INICIA CRONOMETRO PARA TOMAR TIEMPO DE COMPUTO
figure(1)

%INICIALIZACION DE LAS VARIABLES
for i=1:N

    for j=1:PD
        
        for var=1:dim
            xi(var,i,j)=lb+rand*(ub-lb);
        end
        
    end

end

% INICIALIZACION DE VARIABLE CAOTICA
z=rand;
while(z==0.25 || z==0.5 || z==0.75 || z==1)
    z=rand;
end


% ITERACIONES DEL ALGORITMO
for t=1:it
    
    %EVALUACION Y DECLARACION DEL OPTIMO GLOBAL
    for k=1:N
       for l=1:PD
           
            s(k,l)=fobj(xi(:,k,l));
            
            if (s(k,l)<Optimum) %CONDICION PARA MINIMIZAR (INVERTIR PARA MAXIMIZAR)
               Optimum=s(k,l);  %GUARDADO DE MEJOR FITNESS
               for var=1:dim    %GUARDADO DE MEJOR SOLUCION GLOBAL
                   Vx(var)=xi(var,k,l);
               end 
            end   
            
       end 
    end
    
    
    % ACTUALIZACION DE LAS POSICIONES DE LOS BUHOS
    for i=1:N
    
        w=min(s(i,:)); %MEJOR MINIMO DEL ESPACIO DE BUSQUEDA DEL BUHO
        b=max(s(i,:)); %MEJOR MAXIMO DEL ESPACIO DE BUSQUEDA DEL BUHO
    
        for j=1:PD
     
            I=(s(i,j)-w)/(b-w); % RESONANCIA 
            
            %CALCULO DE DISTANCIA E INTENSIDAD POR CADA DIMENSION (VARIABLE)
            for var=1:dim
                Rx(var)=(xi(var,i,j)^2) + (Vx(var)^2); %DISTANCIA EUCLIDIANA
                Icx(var)=I/(Rx(var)^2)+z;              %INTENSIDAD
            end
            
            
            %UPDATE DE POSICION
            if (rand<0.5)
                
                for var=1:dim
                    xi(var,i,j) = xi(var,i,j) + Bi*Icx(var)*abs((0.5*rand*Vx(var))-xi(var,i,j)); 
                    
                    if(xi(var,i,j)<lb || xi(var,i,j)>ub) %VERIFICACION DE NO EXCEDER LIMITES
                       xi(var,i,j)= lb+rand*(ub-lb); 
                    end
                    
                end
               
            else
                
                for var=1:dim
                    xi(var,i,j) = xi(var,i,j) - Bi*Icx(var)*abs((0.5*rand*Vx(var))-xi(var,i,j));
                    
                    if(xi(var,i,j)<lb || xi(var,i,j)>ub) %VERIFICACION DE NO EXCEDER LIMITES
                       xi(var,i,j)= lb+rand*(ub-lb); 
                    end
                    
                end
            
            end
      
           %UPDATE VARIABLE CAOTICA
           
           %LOS COMENTARIOS SON PARA HACER PRUEBAS CON DIFERENTES MAPAS
           %CAOTICOS
           
           z = cos(t*acos(z));
           
           %z=mod(z+0.2-(0.5/(2*pi))*sin(2*pi*z),1);
           
           %z=mod(1/z,1);
           
           %z = sin(0.7*pi/z);
           %z=((z+1))/(2);
           
           %z=4*z*(1-z);
           
           %z = 3*z*(z^2-1)+z;
           
           %z=sin(pi*z);
           
           %z=1.07*(7.86*z-23.31*(z^2)+28.75*(z^3)-13.302875*(z^4));
           
           %z=2.3*z^2*sin(pi*z);
           
%            if z<0.7
%             z=z/0.7;
%            end
%            if z>=0.7
%             z=(10/3)*(1-z);
%            end

%            P=0.4;
%            if z>=0 && z<P
%             z=z/P;
%            end
%            if z>=P && z<0.5
%             z=(z-P)/(0.5-P);
%            end
%            if z>=0.5 && z<1-P
%             z=(1-P-z)/(0.5-P);
%            end
%            if z>=1-P && z<1
%             z=(1-z)/P;
%            end   
           
            
        end
     
    end
    
    %CONSTANTE EN DECRECION LINEAL
    Bi=B-((B/it)*t);
    
    %GRAFICA DE RESPUESTA EN TIEMPO
     optim(t)=Optimum;
      
end
 hold on
 plot(optim ,'k')
 xlabel('iteration');
 ylabel('optimum');
%% RESULTADOS
disp(' ')
disp('---RESULTADOS CHAOTIC OWL SEARCH ALGORITHM---')
fprintf('MINIMO GLOBAL EN EL RANGO: %f \n',Optimum)
fprintf('VALOR DE LAS VARIABLES')
disp(Vx)

toc %MUESTRA TIEMPO DE COMPUTO

end